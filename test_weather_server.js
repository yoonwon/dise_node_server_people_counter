let sampleXML = '<WeatherInfoResponse xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\
<count>4</count>\
<message>success</message>\
<list>\
<WeatherInfo MINTEM="-15" MAXTEM="1" WEATHER="맑음" WIND="3" HUMIDITY="65" CITY_NAME="우파" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
<WeatherInfo MINTEM="-9" MAXTEM="3" WEATHER="맑음" WIND="4" HUMIDITY="44" CITY_NAME="카잔" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
<WeatherInfo MINTEM="-24" MAXTEM="-12" WEATHER="눈또는 비" WIND="9" HUMIDITY="87" CITY_NAME="울란우데" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
<WeatherInfo MINTEM="-20" MAXTEM="-6" WEATHER="비또는 눈" WIND="1" HUMIDITY="95" CITY_NAME="알단" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
<WeatherInfo MINTEM="-9" MAXTEM="1" WEATHER="흐림" WIND="4" HUMIDITY="90" CITY_NAME="하바오브스크" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
<WeatherInfo MINTEM="-4" MAXTEM="8" WEATHER="맑음" WIND="7" HUMIDITY="74" CITY_NAME="블라디보스토크" YOILEN="Wed" YOILKO="수" DATEEN="November 14" DATEKO="11월 14일" WDATE="20181114"/> \
</list>\
</WeatherInfoResponse>\
'

const http = require('http');
const hostname = '127.0.0.1';

const port = 9002;


const server = http.createServer(function (req, res) {
                                 
    var xmlText = "";
                                 
    try {
    
    req.on('data', function (chunk) {
                xmlText += chunk;
           });
    
    req.on('end', function () {
           try {
           
           const { headers, method, url } = req;
           
           console.log(method);
           
           if (method == 'GET') {
           
               res.writeHead(200);
               res.end(sampleXML);
           }

           } catch (exception) {
               console.log(exception);
               res.writeHead(404);
               res.end();
           }
           });
    
    }
    catch (exception) {
    console.log(exception);
    }
    });


server.listen(port, hostname, () => {
              console.log(`Server running at http:${hostname}:${port}/`);
              });
