var net = require('net');

var server = net.createServer(function(socket){
	console.log(socket.address().address + " connected.");
	
	// client로 부터 오는 data를 화면에 출력
	socket.on('data', function(data){
        console.log('rcv0:' + data[0]);
        console.log('rcv1:' + data[1]);
        console.log('rcv2:' + data[2]);
        console.log('rcv3:' + data[3]);

	});
	// client와 접속이 끊기는 메시지 출력
	socket.on('close', function(){
		console.log('client disconnted.');
	});
	// client가 접속하면 화면에 출력해주는 메시지
	var buf = Buffer(4);

	buf.writeUInt8(0x02, 0);
    buf.write('C', 1);
    buf.write('0', 2);
    buf.writeUInt8(0x03, 3);
    socket.write(buf);  
});

// 에러가 발생할 경우 화면에 에러메시지 출력
server.on('error', function(err){
	console.log('err'+ err	);
});

// Port 5000으로 접속이 가능하도록 대기
server.listen(5000, function(){
	console.log('linsteing on 5000..');
});