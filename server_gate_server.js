
//텍스트가 생성되는 기본 위치
var path = "C:\\DISEContent\\disehm\\";
//텍스트안에 이미지 파일 기본 파일 패스
var pathImg = "C:\\DISEContent\\disehm\\weather\\";

//npm install http
//npm install fs
//npm install xml2json
//npm install request


var request = require('request');
const http = require('http');
var fs = require('fs');
var xmlparser = require('xml2json');

//메인 서버는 처음 데이터를 가져오는 서버 
//만약 서브 호스트 서버라면 const isMainServer = false; 로 설정해야 합니다.  
const isMainServer = true; 

//서브 호스트는 메인서버에서 데이터를 받아 저장만 하는 서버 
const isSubHostServer = true; 

//이 컴퓨터에서 서버로 동작할 아이피와 포트 (방화벽에서 열어야함.)
const hostname = '172.21.14.166';
const port = 9001;

//XML 데이터를 받아오는 데이터 서버 상세한 주소가 필요함
const originalXmlServer = "http://172.24.13.236:8080/";

//받아온 데이터를 나눠 받을 서브호스트 서버의 아이피와 포트 
var subHosts =  ["http://172.21.14.167:9001", "http://172.21.14.168:9001", "http://172.22.13.54:9001", "http://172.22.13.55:9001"];


//필터링할 나라 또는 도시 이름
var citiFiltering = [];


var subHostReturnCount = 0;

var originalJson ;
var receivedJson ;

// WDATE: '20181022',
//        YOIL: '월',
//        CITY_NAME: '덴버',
//        HUMIDITY: '42',
//        WIND: '5',
//        WEATHER: '맑음',
//        MAXTEM: '23',
//        MINTEM: '2' 

function engWeatherImgPath(WEATHER) {
    var engWeather = pathImg + "default.png";

    if (WEATHER == "맑음") {
        engWeather = pathImg + "sunny.png";
    } else if (WEATHER == "흐림") {
        engWeather = pathImg + "cloudy.png";
    } else if (WEATHER == "눈") {
        engWeather = pathImg + "snow.png";
    } else if (WEATHER == "비") {
        engWeather = pathImg + "rain.png";
    } else {
        engWeather = pathImg + "default.png";
    }

    return engWeather;
}

function saveFile(WDATE, YOIL, CITY_NAME, HUMIDITY, WIND, WEATHER, MAXTEM, MINTEM) {
    
    var cityPath = path + CITY_NAME;

    var path_WDATE = cityPath + "_WDATE.txt";
    fs.writeFileSync(path_WDATE, WDATE);
    
   var path_YOIL = cityPath + "_YOIL.txt";
   fs.writeFileSync(path_YOIL, YOIL);
    
    var path_HUMIDITY = cityPath + "_HUMIDITY.txt";
    fs.writeFileSync(path_HUMIDITY, HUMIDITY);

    var path_WIND = cityPath + "_WIND.txt";
    fs.writeFileSync(path_WIND, WIND);

    var path_WEATHER = cityPath + "_WEATHER.txt";
    fs.writeFileSync(path_WEATHER, engWeatherImgPath(WEATHER));

    var path_MAXTEM = cityPath + "_MAXTEM.txt";
    fs.writeFileSync(path_MAXTEM, MAXTEM);

    var path_MINTEM = cityPath + "_MINTEM.txt";
    fs.writeFileSync(path_MINTEM, MINTEM);
    
    console.log('File Write Done  : ' + CITY_NAME);
}


function SaveWeatherJson ()
{
    console.log("SaveWeatherJson");
    var cityList = receivedJson.WeatherInfoResponse.list.WeatherInfo;

    for (i = 0 ; i < cityList.length ; i++) {
        var city = cityList[i];

        var found = citiFiltering.find(function(element) {
            if (element == city.CITY_NAME) {
                return true ;
            } else {
                return false;
            }
          });
          
        if (found) {
            saveFile(city.WDATE, city.YOIL, city.CITY_NAME, city.HUMIDITY, city.WIND, city.WEATHER ,city.MAXTEM, city.MINTEM);
            console.log("Saved : " + city.CITY_NAME);
        }
     }
}

function GetWeatherJSon( parsedData )
{
    try {
        console.log('-----------------START RECEIVING---------------------');
        
        receivedData = parsedData;                
        
        console.log('---------------SUCCESS RECEIVED-------------------');
        
        
    } catch (exception) {
        
        console.log('---------- FAILED JSON RECEIVING ----------');
        
    }
}


function InitHostData() {
    subHostReturnCount = 0;
}

function ReceviedSubHost( ) {

    console.log ("ReturnCount : " + subHostReturnCount);
    subHostReturnCount =  subHostReturnCount + 1;

    var isReceivedAll = false;
    if (subHostReturnCount >= subHosts.length) {
        isReceivedAll = true;
    }

    if (isReceivedAll) {
        console.log ("ReturnCount : Full");
        XmlComplete();
    }
}

function XmlComplete() {

    SaveWeatherJson();
    
    for (i = 0; i < subHosts.length; i++) { 
        var subhost = subHosts[i];
        SendSubHostComplete(subhost);
    } 

}

function SendSubHostComplete(url) {
    
    console.log("Send Subhost : Complete : " + url);

    request({
        url: url + "/request_complete",
        method:"POST",
        json:true},function(error,response,body){
          
            console.log("Send Subhost : Complete : End : " + url);
            
      }
    );
}

function SendSubHost(url, json) {
    
    console.log("Send Subhost : " + url);

    request({
        url: url + "/request_begin",
        method:"POST",
        json:originalJson}, function(error,response,body){

            if (body == 'request_ok') {
                console.log("Send Subhost : End " + url);
                ReceviedSubHost();
            } else {
                console.log("Send Subhost : Fail " + url);
            }
      }
    );
}

function GetWeatherXMLTimer() {

    InitHostData();   

    request({
    url: originalXmlServer,
    method:"GET",
    json:originalJson}, function(error,response,body){
    
        try {
            console.log('-----------------START---------------------');
            
            var jsonData = xmlparser.toJson(body);
            var jsonobj = JSON.parse(jsonData);
            
            originalJson = jsonobj;
    
            for (i = 0; i < subHosts.length; i++) { 
                var subhost = subHosts[i];
                SendSubHost(subhost, jsonobj);
            }                                     
            
            } catch (exception) {
            
                console.log('---------- FAILED XML REQUEST ----------');
            
            }
        });
}


if (isMainServer) {
    setInterval(GetWeatherXMLTimer, 1000*30);
} 

if (isSubHostServer) {

    const server = http.createServer(function (req, res) {
                                 
        var jsonData = "";
                                     
        try {
        
        req.on('data', function (chunk) {
            jsonData += chunk;
               });
        
        req.on('end', function () {
               try {
               
               const { headers, method, url } = req;
               
               console.log(method);
               
               if (method == 'GET') {
               
                   res.writeHead(200);
                   res.end(url + ' OK');
    
               } else {
    
                   if (url == '/request_begin') {
                      
                        console.log("Received Subhost : " + jsonData);
                        receivedJson =  JSON.parse(jsonData);
    
                       res.writeHead(200);
                       res.end('request_ok');
    
                   } else if (url == '/request_complete') {
                      
                     console.log("Received Subhost : Complete ");
    
                       SaveWeatherJson();
    
                       res.writeHead(200);
                       res.end('request_saved');
                   } else {
                       res.writeHead(200);
                       res.end(url + ' OK');
                   }
    
               }
               
               } catch (exception) {
                   console.log(exception);
                   res.writeHead(404);
                   res.end();
               }
               });
        
        }
        catch (exception) {
        console.log(exception);
        }
        });
    
    server.listen(port, hostname, () => {
                  console.log(`Server running at http:${hostname}:${port}/`);
                  });
                             
    }

 

