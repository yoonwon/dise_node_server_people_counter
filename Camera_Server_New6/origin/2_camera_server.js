﻿const http = require('http');
var fs = require('fs');

const hostname = '172.22.13.1';
const port = 9001;

var languangeTimer = 5.0 * 1000.0;

//텍스트가 생성되는 기본 위치
var path = "C:\\DISEContent\\disehm\\";

//텍스트안에 이미지 파일 기본 파일 패스
var pathImg = "C:\\DISEContent\\disehm\\images\\";

var globalStoredArray = Array();

globalStoredArray[0] = Array();
globalStoredArray[1] = Array();
globalStoredArray[2] = Array();
globalStoredArray[3] = Array();
globalStoredArray[4] = Array();
globalStoredArray[5] = Array();
globalStoredArray[6] = Array();
globalStoredArray[7] = Array();
globalStoredArray[8] = Array();
globalStoredArray[9] = Array();

var languangeIndex = 0;
var lanuanges = Array();

lanuanges[0] = "koen";
//lanuanges[1] = "cnjp";
//lanuanges[2] = "jp";

function timerWrite() {
    
    //언어 추가 "cn" : 중국어, "jp" : 일본어, "ko" : 한국어 등등.
    if (languangeIndex >= lanuanges.length) {
        languangeIndex = 0;
        console.log("laung restart") ;
    }
    
    saveFileByLanguage(lanuanges[languangeIndex], globalStoredArray);
    console.log(languangeIndex + " " + lanuanges[languangeIndex]) ;
    
    languangeIndex += 1;

}

setInterval(timerWrite, languangeTimer);


//var jsonString = '{
//"sent": "2018-04-25 10:00:00 + 0900",
//"events": [
//           {
//           "GateID": 20,
//           "CurrentStatus": 2,
//           "Meter": 15
//           },
//           {
//           "GateID": 21,
//           "CurrentStatus": 1,
//           "Meter": 10
//           },
//           {
//           "GateID": 22,
//           "CurrentStatus": 2,
//           "Meter": 20
//           },
//           {
//           "GateID": 30,
//           "CurrentStatus": 3,
//           "Meter": 30
//           },
//           {
//           "GateID": 31,
//           "CurrentStatus": 2,
//           "Meter": 20
//           },
//           {
//           "GateID": 32,
//           "CurrentStatus": 4,
//           "Meter": 40
//           }
//           ]
//}';

//게이트 룰 설정
function statusFromGate(newIndex, contents) {
    
    var status = contents;
    
    if (status == 1) {
        statusString = pathImg + "gate" + newIndex + "_notbusy";
    } else if (status == 2) {
        statusString = pathImg + "gate" + newIndex + "_lessbusy";
    } else if (status == 3) {
        statusString = pathImg + "gate" + newIndex + "_busy";
    } else if (status == 4) {
        statusString = pathImg + "gate" + newIndex + "_verybusy";
    } else if (status == 9) {
        statusString = pathImg + "gate" + newIndex + "_closed";
    } else {
        statusString = pathImg + "gate" + newIndex + "_closed";
    }
    
    return statusString;
}


//인자값 : 게이트번호, 상태정보, 스타일이름, 이미지 파일타입
function saveFile(gateIndex, statusString, watingMeter, style, filetype) {
    
    var fileImgPath = path + "dep_" + gateIndex.toString() + "_img_"  + style + ".txt";
    fs.writeFileSync(fileImgPath, statusString + "_" + style + "." + filetype);
    
    var fileTextPath = path + "dep_" + gateIndex.toString() + "_meter"  + ".txt";
    fs.writeFileSync(fileTextPath, watingMeter + "M");

    console.log('File Write Done  : ' + fileImgPath);
}


function saveFileByLanguage(language, array) {
    
    for ( index in array) {
        
        var cameraDataArray = array[index];
        
        for (sub_index in cameraDataArray) {
            
            var cameraData = cameraDataArray[sub_index];

            var newIndex = Math.floor(parseInt(cameraData.GateID) / 10);
            var subIndex = parseInt(cameraData.GateID) % 10;
            var watingMeter = parseInt(cameraData.Meter);
            
            var contents = cameraData.CurrentStatus;
            
            if (newIndex >= 2 && newIndex <= 5) {
                
                var statusString = 'default';
                
                //게이트 별 거리에 따른 타입 지정
                statusString = statusFromGate(newIndex, contents) + "_" + language;
                
                console.log("GateID : " + newIndex + "SubID : " + subIndex + ", CurrentStatus : " + contents + ", status : " + statusString + ", meter : " + watingMeter ) ;
                
                //스타일 예시, st1, st2, st3
                //이미지 파일 확장자 지정 예시 png
                
                saveFile(cameraData.GateID, statusString, watingMeter, "st1", "png");
                saveFile(cameraData.GateID, statusString, watingMeter, "st2", "png");
                //            saveFile(newIndex, statusString, watingMeter, "st3", "png");
                //            saveFile(newIndex, statusString, watingMeter, "st4", "png");
                
            } else {
                console.log("No GateID : " + cameraData.GateID);
            }
        }
    }
}


function GetCameraJSon( parsedData )
{
    try {
        console.log('-----------------START RECEIVING---------------------');
        
        var array = parsedData.events;
        
        for ( index in array) {
            
            var cameraData = array[index];
            
            var newIndex = Math.floor(parseInt(cameraData.GateID) / 10);
            var subindex = parseInt(cameraData.GateID) % 10;
            var watingMeter = parseInt(cameraData.Meter);
            
            var contents = cameraData.CurrentStatus;
            
            globalStoredArray[newIndex][subindex] = cameraData;
            
        }
        
        console.log('---------------SUCCESS RECEIVED-------------------');
        
        
    } catch (exception) {
        
        console.log('---------- FAILED JSON RECEIVING ----------');
        
    }
}


const server = http.createServer(function (req, res) {
                                 
                                 var jsonData = "";
                                 
                                 try {
                                 
                                 req.on('data', function (chunk) {
                                        jsonData += chunk;
                                        });
                                 
                                 req.on('end', function () {
                                        try {
                                        
                                        const { headers, method, url } = req;
                                        
                                        console.log(method);
                                        
                                        if (method == 'GET') {
                                        
                                            res.writeHead(200);
                                            res.end(url + ' OK');

                                        } else {
                                                var reqObj = JSON.parse(jsonData);
                                        
                                                GetCameraJSon (reqObj);
                                        
                                                console.log(jsonData);

                                                res.writeHead(200);
                                                res.end('OK : ' + jsonData);
                                      
                                        }
                                        
                                        } catch (exception) {
                                            console.log(exception);
                                            res.writeHead(404);
                                            res.end();
                                        }
                                        });
                                 
                                 }
                                 catch (exception) {
                                 console.log(exception);
                                 }
                                 });


server.listen(port, hostname, () => {
              console.log(`Server running at http:${hostname}:${port}/`);
              });