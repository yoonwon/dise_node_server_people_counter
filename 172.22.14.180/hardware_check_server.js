var net = require('net');
var dialog = require('dialog');
var request = require('request');
var fs = require('fs');

var setupFile = JSON.parse(fs.readFileSync('setup.json', 'utf8'));

// 서버 5000번 포트로 접속
// 172.21.14.185:9010
//C 0 0번 3대 PC 
//C 1 1번 1대 PC 
//P 주기적 10초에 한번 

// var liveServerList = ["http://172.21.14.166:9001", "http://172.21.14.167:9001", "http://172.21.14.168:9001"];
var liveServerList = ["http://127.0.0.1:9001"];
var liveSecs = 10;

var isManualSettingDisplay = 49; //49 manuall 48 auto

var tryingCountDialog = 0;
var isShownDialog = false;

var dispayHardwareHost = '172.21.14.185';
var dispayHardwareHostPort = 9010;

var isRunning = false;
var deadCountArray = Array(3);
let deadCriticalNumber = 3;


if (setupFile) {

    console.log ("SetupFile Load");
    console.log (setupFile);
    console.log ("");

    dispayHardwareHost = setupFile.dispayHardwareHost;
    dispayHardwareHostPort = setupFile.dispayHardwareHostPort;

    liveServerList = setupFile.liveServerList;
    liveSecs = setupFile.liveSecs;

    deadCountArray = Array(liveServerList.length);

}

resetDeadCount();

var socket = net.connect({port: dispayHardwareHostPort, host: dispayHardwareHost});
// var socket = net.connect({port: 5000, host: 'localhost'});

socket.on('connect', function(){
	console.log('connected to server!');

    readySetup();

});

function checkClient(url) {
    
    console.log("Send Start " + url);

    request({
        url: url+'/live',
        method:"GET", 
        timeout: liveSecs-1}, function(error,response,body){

            if (body == 'live') {
                console.log(url + " live");

                var indexOfLive = liveIndex(url);
                updateLiveCountCheck(indexOfLive);

            } else {
                console.log(url + " dead!!!!!");
                
                var indexOfDead = deadIndex(url);
                var shouldAlert = updateDeadCountCheck(indexOfDead);

                if (isManualSettingDisplay == 49) {
                    if (shouldAlert == true && isShownDialog == false) {
                        dialog.info(url + '\n디스플레이의 이상이 발견되었습니다. \n수동으로 화면을 전환해 주세요.');
                        isShownDialog = true;

                        resetDeadCount();
                    }
                    console.log("Manual Display Dialog Show : " + tryingCountDialog);

                } else {
                    
                    if (shouldAlert == true && isShownDialog == false) {
                        dialog.info(url + '\n디스플레이의 이상이 발견되었습니다. \n자동으로 화면이 전환됩니다.');
                        sendSetDiplay1();
                        isShownDialog = true;

                        resetDeadCount();
                    }
                    console.log("Auto Display Dialog Show : " + tryingCountDialog);
                }
            }
        }
    );
}

function resetDeadCount()
{
    for ( i = 0; i < deadCountArray.length ; i++) {
        deadCountArray[i] = 0;
    }   
}

function updateLiveCountCheck(indexOfDead)
{
    deadCountArray[indexOfDead] = 0;
}

function liveIndex(url) {
    return deadIndex(url);
}

function deadIndex(url) {

    var indexOfDead = -1;

    for ( i = 0; i < liveServerList.length ; i++) {
        var server = liveServerList[i];
        
        if (url == server) {
            indexOfDead = i;
            break;
        }
    }

    return indexOfDead;
}

function updateDeadCountCheck(indexOfDead) {

    for ( i = 0; i < deadCountArray.length ; i++) {
        deadCountArray[i] += 1;
        
        console.log(liveServerList[indexOfDead] + " : updateDeadCountCheck : " + i + " " + deadCountArray[i]);

        if (deadCountArray[i] >= deadCriticalNumber) {
            return true;
        }
    }   

    return false;
}

function sendPolling()
{
    var buf = new Buffer(4);

    buf.writeUInt8(0x02, 0);
    buf.write('P', 1);
    buf.write('0', 2);
    buf.writeUInt8(0x03, 3);
    socket.write(buf);  
}

function sendSetDiplay0()
{
    var buf = new Buffer(4);

    buf.writeUInt8(0x02, 0);
    buf.write('C', 1);
    buf.write('0', 2);
    buf.writeUInt8(0x03, 3);
    socket.write(buf);  
}

function sendSetDiplay1()
{
    var buf = new Buffer(4);

    buf.writeUInt8(0x02, 0);
    buf.write('C', 1);
    buf.write('1', 2);
    buf.writeUInt8(0x03, 3);
    socket.write(buf);  
}



// 서버로부터 받은 데이터를 화면에 출력
socket.on('data', function(chunk){

    console.log('recv 0:' + chunk[0]);
    console.log('recv 1:' + chunk[1]);
    console.log('recv 2:' + chunk[2]);

    if (chunk[1] == 80) { //'P' = 80
        isManualSettingDisplay = chunk[2];
        console.log('isManualSettingDisplay : ' + isManualSettingDisplay);
      
        if (isRunning == false) {
            console.log('Start Service...');
            startService();
            isRunning = true;
        }    
    }

    console.log('recv 3:' + chunk[3]);
});

// 접속이 종료됬을때 메시지 출력
socket.on('end', function(){
	console.log('disconnected.');
});
// 에러가 발생할때 에러메시지 화면에 출력
socket.on('error', function(err){
    console.log(err);
    startService();
});
// connection에서 timeout이 발생하면 메시지 출력
socket.on('timeout', function(){
	console.log('connection timeout.');
});

function readySetup()
{
    console.log('Send Ready Polling');

    isRunning = false;
    sendPolling();

}

function startService() {
    setInterval(function(){

        sendPolling();

        for (i = 0; i < liveServerList.length; i++) {
            var hostURL = liveServerList[i];
            checkClient(hostURL);
        }

        if (tryingCountDialog > 60) {
            
            isShownDialog = false;
            tryingCountDialog = 0;
        }

        tryingCountDialog += 1;
        
    }, liveSecs * 1000);
}