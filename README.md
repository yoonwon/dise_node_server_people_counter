# README #

인천 공항 밀레니엄 서버 연동 Node JS Server

### 구성 ###

1. 메인 서버 
	XML 서버로 부터 데이터를 받아 서브 호스트 2대에 나눠주는 역할
	소스 코드의 isMainServer = true; 로 사용함 
	
2. 서브 호스트 2대 
	수동적으로 메인서버에서 데이터를 주는 것을 받아 저장 하는 역할만 함.
	소스 코드의 isMainServer = false; 로 변경하여 사용함


### 설치방법 ###

1. 노드 Js 윈도우용 설치 
https://nodejs.org/ko/ 

2. 노드 js 확장 프로그램 설치 
  윈도우 버튼 + R (실행창) 
  cmd 실행
  
  cmd 창에서 명령어 실행
  //인터넷이 있다면 
  npm install -g express http file-system xml-js request
  
  //인터넷이 없다면 
 다음의 파일을 npm_package 폴더를 C:\DISEContent\disehm 에 복사 후 
 실행
 npm i -g server_weather-1.0.0.tgz
 실행후 실행하려는 js 파일을 server_weather 폴더에 넣고 저장 

 
  
3. 설치 후 테스트 
   1. C:\DISEContent 폴더 하위에 disehm 폴더 생성
   2. C:\DISEContent\disehm 폴더에 server_weather.js 파일 복사 (파일열어서 실제 아이피, 포트 설정해줘야함) 
   3. C:\DISEContent\disehm\images 폴더에 이미지 저장 
   4. server_weather.js 파일에서 수정해야될 사항들 

      메인 서버 (LED 1번 PC 만 메인 서버, 나머지는 isMainServer = false; 로 설정해야함)
	  isMainServer = true; 
	  
	  서브 호스트 
	  isMainServer = false;
	  
      이 컴퓨터에서 서버로 동작할 아이피와 포트 (방화벽에서 열어야함.) 
	  //LED 1번 PC의 예
      hostname = '172.21.14.166'; 
      port = 9001;
	  
	  XML 데이터를 받아오는 데이터 서버 상세한 주소가 필요함
      
	  originalXmlServer = "http://172.24.13.236:8080/";


      받아온 데이터를 나눠 받을 서브호스트 서버의 아이피와 포트 
      
	  subHosts = ["http://172.21.14.167:9001", "http://172.21.14.168:9001", "http://172.22.13.54:9001", "http://172.22.13.55:9001"];
	  
	  필터링할 나라 또는 도시 이름
      
      var citiFiltering = ["중국", "일본", "베트남"];
	  
3. 윈도우 서비스 등록 

  nssm install server_weather

  nssm 윈도우에서 다음의 값으로 설정 

  path c:\program files\nodejs\node.exe
  Startup directory c:\program files\nodejs
  Aguments c:\disecontent\disehm\server_weather.js
   
  service name server_weather
   
  install service 버튼 클릭으로 마무리 
   
   
