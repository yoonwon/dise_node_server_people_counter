//텍스트가 생성되는 기본 위치
var path = "C:\\DISEContent\\disehm\\";
//텍스트안에 이미지 파일 기본 파일 패스
var pathImg = "C:\\DISEContent\\disehm\\weather\\";

//npm install http
//npm install fs
//npm install xml2json
//npm install request


var request = require('request');
const http = require('http');
var fs = require('fs');
var xmlparser = require('xml-js');

var ntpClient = require('ntp-client');
var moment = require('moment');

//메인 서버는 처음 데이터를 가져오는 서버 
//만약 서브 호스트 서버라면 const isMainServer = false; 로 설정해야 합니다.  
var isMainServer = true; 

//서브 호스트는 메인서버에서 데이터를 받아 저장만 하는 서버 
var isSubHostServer = true; 

//이 컴퓨터에서 서버로 동작할 아이피와 포트 (방화벽에서 열어야함.)
var hostname = '172.21.14.166';
var port = 9001;


var refreshSecs = 30;
var refreshWeatherSecs = 60*60*1;

//XML 데이터를 받아오는 데이터 서버 상세한 주소가 필요함
var originalXmlServer = "http://127.0.0.1:9002/";

//받아온 데이터를 나눠 받을 서브호스트 서버의 아이피와 포트 
var subHosts =  [];

//, "http://172.22.13.54:9001", "http://172.22.13.55:9001"


//필터링할 나라 또는 도시 이름
var citiFiltering = ["베이징", "도쿄", "하노이", "뉴욕", "블라디보스토크", "파리", "로마", "런던", "아부다비", "캔버라"];

var isUseTimeZone = true;
var ntpServer = "time.bora.net";
var timezones = [{"서울" : "+09:00"}];
var dateFormat = "YYYY-MM-DD HH:mm";

var setupFile = JSON.parse(fs.readFileSync('setup.json', 'utf8'));
var dateEN = "MMM Do YY";
var dataKO = "YYYY년 MM월 DD일";

var sunRise = 6;
var sunSet = 19;

var currentHour = -1;
var updateTimerHour = 10;

var isGetForFirstWeatherRequest = false; 

if (setupFile) {

    console.log ("SetupFile Load");
    console.log (setupFile);
    console.log ("");

    isMainServer = setupFile.isMainServer;
    isSubHostServer = setupFile.isSubHostServer;
    hostname = setupFile.hostIP;
    port = setupFile.hostPort;
    originalXmlServer = setupFile.XMLServer;
    subHosts = setupFile.supHosts;
    citiFiltering = setupFile.citiFiltering;
    refreshSecs = setupFile.refreshSecs;
    ntpServer = setupFile.ntpServer;
    timezones = setupFile.timezones;
    dateFormat = setupFile.dateFormat;
    isUseTimeZone = setupFile.isUseTimeZone;
    dateEN = setupFile.dateEN;
    dateKO = setupFile.dateKO;
    sunRise = setupFile.sunRise;
    sunSet = setupFile.sunSet;

    updateTimerHour = setupFile.updateTimerHour;
    refreshWeatherSecs = setupFile.refreshWeatherSecs;

}

var subHostReturnCount = 0;

var originalJson ;
var receivedJson ;


// WDATE: '20181022',
//        YOIL: '월',
//        CITY_NAME: '덴버',
//        HUMIDITY: '42',
//        WIND: '5',
//        WEATHER: '맑음',
//        MAXTEM: '23',
//        MINTEM: '2' 

function engWeatherImgPath(WEATHER) {
    var engWeather = pathImg + "default.png";

    if (WEATHER == "맑음") {
        engWeather = pathImg + "sunny.png";
    } else if (WEATHER == "흐림" || WEATHER == "구름조금" || WEATHER == "구름많음" || WEATHER == "흐리고 비") {
        engWeather = pathImg + "cloudy.png";
    } else if (WEATHER == "눈" || WEATHER == "눈또는 비") {
        engWeather = pathImg + "snow.png";
    } else if (WEATHER == "비" || WEATHER == "비또는 눈" || WEATHER == "소나기") {
        engWeather = pathImg + "rain.png";
    } else {
        engWeather = pathImg + "default.png";
    }

    return engWeather;
}

function saveFile(city) {
    
    var cityPath = path + city.CITY_NAME;

    for(var key in city) {

        var keyPath = cityPath + "_" + key + ".txt";
        var textValue = city[key];

        if (key == "WEATHER") {
            textValue = engWeatherImgPath(city[key]);
        }
        // console.log(city.CITY_NAME + '[' + key +'] = ' + textValue);

        fs.writeFileSync(keyPath, textValue);
    }
    
    console.log('File Write Done  : ' + city.CITY_NAME);
}

function saveTimeZone(cityName, time) {

    var cityPath = path + cityName;

    var keyPath = cityPath + "_TIMEZONE.txt";
    fs.writeFileSync(keyPath, time);    

}

function saveDateEN(cityName, date) {

    var cityPath = path + cityName;

    var keyPath = cityPath + "_DATE_EN.txt";
    fs.writeFileSync(keyPath, date);    

}

function saveDateKO(cityName, date) {

    var cityPath = path + cityName;

    var keyPath = cityPath + "_DATE_KO.txt";
    fs.writeFileSync(keyPath, date);    

}

function saveDaylight(cityName, day) {
    var cityPath = path + cityName;

    var keyPath = cityPath + "_DAYLIGHT.txt";
    fs.writeFileSync(keyPath, day);    
}


function SaveWeatherJson ()
{
    
    console.log("");

    var cityList = receivedJson.WeatherInfoResponse.list.WeatherInfo;

    // for (i = 0 ; i < cityList.length ; i++) {

    //     var city = cityList[i]._attributes;
    //     var found = true;
    //     if (citiFiltering.length > 0) {

    //         var found = citiFiltering.find(function(element) {
    //             if (element == city.CITY_NAME) {
    //                 return true ;
    //             } else {
    //                 return false;
    //             }
    //           });
    //     }
          
    //     if (found) {
    //         saveFile(city);
    //         console.log("Saved : " + city.CITY_NAME);
    //     }
    //  }

    for (i = 0 ; i < citiFiltering.length ; i++) {

        var cityFilterName = citiFiltering[i];
        var found = true;

        var cityFound = '';

        for (j = 0 ; j < cityList.length ; j++) {
           
            var city = cityList[j]._attributes;

            if (cityFilterName == city.CITY_NAME) {
                found =  true ;
                cityFound = city;
                break;
            } else {
                found = false;
                cityFound = '';
            }
        }
          
        if (found) {
            saveFile(cityFound);
            console.log("Saved : " + cityFound.CITY_NAME);
        }
     }
     
     console.log("");

}

function GetWeatherJSon( parsedData )
{
    try {
        console.log('-----------------START RECEIVING---------------------');
        
        receivedData = parsedData;                
        
        console.log('---------------SUCCESS RECEIVED-------------------');
        
        
    } catch (exception) {
        console.log(exception);
        console.log('---------- FAILED JSON RECEIVING ----------');
        
    }
}


function InitHostData() {
    subHostReturnCount = 0;
}

function ReceviedSubHost( ) {

    console.log ("ReturnCount : " + subHostReturnCount);
    subHostReturnCount =  subHostReturnCount + 1;

    var isReceivedAll = false;
    if (subHostReturnCount >= subHosts.length) {
        isReceivedAll = true;
    }

    if (isReceivedAll) {
        console.log ("ReturnCount : Full");
        XmlComplete();
    }
}

function XmlComplete() {

    SaveWeatherJson();
    
    for (i = 0; i < subHosts.length; i++) { 
        var subhost = subHosts[i];
        SendSubHostComplete(subhost);
    } 

}

function SendSubHostComplete(url) {
    
    isGetForFirstWeatherRequest = true;

    console.log("Send Subhost : Complete : " + url);

    request({
        url: url + "/request_complete",
        method:"POST",
        json:true},function(error,response,body){
          
            console.log("Send Subhost : Complete : End : " + url);
            
      }
    );
}

function SendSubHost(url, json) {
    
    console.log("Send Subhost : " + url);

    request({
        url: url + "/request_begin",
        method:"POST",
        json:originalJson}, function(error,response,body){

            if (body == 'request_ok') {
                console.log("Send Subhost : End " + url);
                ReceviedSubHost();
            } else {
                console.log("Send Subhost : Fail " + url);
            }
      }
    );
}

function ConvertDayOfWeekEnToKo(dayEn) {
    
    var korean = "";
    
    if (dayEn == "Mon") {
        korean = "월요일";
    } else if (dayEn == "Tue") {
        korean = "화요일";
    } else if (dayEn == "Wed") {
        korean = "수요일";
    } else if (dayEn == "Thu") {
        korean = "목요일";
    } else if (dayEn == "Fri") {
        korean = "금요일";
    } else if (dayEn == "Sat") {
        korean = "토요일";
    } else if (dayEn == "Sun") {
        korean = "일요일";
    } else  {
        korean = "";
    } 

    return korean;
}

function GetTimeZone() {
        
    ntpClient.getNetworkTime(ntpServer, 123, function(err, date) {
        
        if(err) {
            console.error(err);
            return;
        }
    
        var utcMoment = moment(date);
        var seoulHour = parseInt(utcMoment.utcOffset("+08:00").format("HH"));
        currentHour = seoulHour;

        for ( i = 0 ; i < timezones.length ; i++) {

            var timezone = timezones[i];

            for (key in timezone) {
                var cityName = key;
                var tzOffset = timezone[cityName];

                var dateKOConverted = dateKO;

                if (dateKO.includes("ddd")) {

                    var dayEn = utcMoment.utcOffset(tzOffset).format("ddd");
                    var dayKo = ConvertDayOfWeekEnToKo(dayEn);

                    dateKOConverted = dateKO.replace("ddd", dayKo);
                }

                // console.log(cityName + " " + tzOffset + " : " +utcMoment.utcOffset(tzOffset).format(dateFormat));
                var timeString = utcMoment.utcOffset(tzOffset).format(dateFormat);
                var dateEnString = utcMoment.utcOffset(tzOffset).format(dateEN);
                var dateKoString = utcMoment.utcOffset(tzOffset).format(dateKOConverted);

                var daylight = "DAY"; //"NIGHT"

                var currentTime = parseInt(utcMoment.utcOffset(tzOffset).format("HH"));
                
                if (currentTime >= sunRise && currentTime <= sunSet ) {
                    daylight = "DAY";
                } else {
                    daylight = "NIGHT";
                }

                saveTimeZone(cityName, timeString.toUpperCase());
                saveDateEN(cityName, dateEnString.toUpperCase());
                saveDateKO(cityName, dateKoString)
                
                saveDaylight(cityName, daylight);

            }
        }

        console.log('');
        console.log('File TimeZone Write Done');
        console.log('');

    });
}

function GetWeatherXMLTimer() {

    if (isGetForFirstWeatherRequest != false) {

        if (currentHour == updateTimerHour) {
            //PASS only 10 am
            console.log("GetWeatherXMLTimer Work at : " + currentHour );
        } else {
            console.log("GetWeatherXMLTimer Not Work at : " + currentHour + ", will work at : " + updateTimerHour);
            return;
        }
    } else {
        console.log("GetWeatherXMLTimer Work First" );
    }


    InitHostData();   

    request({
        url: originalXmlServer,
        method:"GET",
        json:originalJson}, 
        function(error,response,body){
        
            try {
                console.log('-----------------START---------------------');
                
                var jsonData = xmlparser.xml2json(body, {compact: true, spaces: 4});
                var jsonobj = JSON.parse(jsonData);
                
                originalJson = jsonobj;
                receivedJson = originalJson;
    
                //서브호스트가 없으면 데이터를 저장하고 끝낸다. 
                if (subHosts.length == 0) {
                    XmlComplete();
                    isGetForFirstWeatherRequest = true;
                }
                
                for (i = 0; i < subHosts.length; i++) { 
                    var subhost = subHosts[i];
                    SendSubHost(subhost, jsonobj);
                }                                     
                
                } catch (exception) {
                    console.log(exception);
                    console.log('---------- FAILED XML REQUEST ----------');
                
                }
        });
}

if (isUseTimeZone) {
    setInterval(GetTimeZone, 1000*refreshSecs);
}

if (isMainServer) {
    //1시간에 한번 갱신 
    isGetForFirstWeatherRequest = false;
    GetWeatherXMLTimer();
    setInterval(GetWeatherXMLTimer, 1000*refreshWeatherSecs);
} 

if (isSubHostServer) {

    const server = http.createServer(function (req, res) {
                                 
        var jsonData = "";
                                     
        try {
        
        req.on('data', function (chunk) {
            jsonData += chunk;
               });
        
        req.on('end', function () {
               try {
               
               const { headers, method, url } = req;
               
               console.log(method);
               
               if (method == 'GET') {
               
                if (url == '/live') {
                    console.log ('Received live signal');

                    res.writeHead(200);
                    res.end('live');
                } else {
                   res.writeHead(200);
                   res.end(url + ' OK');
                }
    
               } else {
    
                   if (url == '/request_begin') {
                      
                        console.log("Received Subhost : " + jsonData);
                        receivedJson =  JSON.parse(jsonData);
    
                       res.writeHead(200);
                       res.end('request_ok');
    
                   } else if (url == '/request_complete') {
                      
                     console.log("Received Subhost : Complete ");
    
                       SaveWeatherJson();
    
                       res.writeHead(200);
                       res.end('request_saved');
                   } else {
                       res.writeHead(200);
                       res.end(url + ' OK');
                   }
    
               }
               
               } catch (exception) {
                   console.log(exception);
                   res.writeHead(404);
                   res.end();
               }
               });
        
        }
        catch (exception) {
        console.log(exception);
        }
        });
    
    server.listen(port, hostname, () => {
        console.log("");
        console.log(`Server running at http:${hostname}:${port}/`);
        console.log("");
        });
                             
    }

 

